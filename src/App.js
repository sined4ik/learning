import React from "react";
import Button from "./components/button";
import "./App.css";
import Fetch from "./fetch";
import FetchHook from "./fetch-hook";

class App extends React.Component {
  state = {
    count: 1,
    isHide: true
  };

  toggle = () => {
    this.setState(prevState => ({ isHide: !prevState.isHide }));
  };

  render() {
    const { count, isHide } = this.state;

    return (
      <>
        <Fetch />
        <FetchHook />
      </>
    );
  }
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <Button variant="contained" color="primary" disableElevation>
//           Disable elevation
//         </Button>
//         <button className="styled-button"> CLick</button>
//         <button id="second">second</button>
//         <input value="3213213" />
//       </header>
//     </div>
//   );
// }

export default App;
