import React, { Component } from "react";

export default class Fetch extends Component {
  state = {
    data: []
  };

  fetchApi = async () => {
    try {
      const data = await fetch("https://jsonplaceholder.typicode.com/posts");
      const normalizeData = await data.json();
      this.setState({ data: normalizeData });
    } catch (error) {}
  };

  componentDidMount() {
    this.fetchApi();
  }

  render() {
    const { data } = this.state;
    return (
      <div>
        <ul>
          {/* {data.map((item, idx) => {
            return <li key={item.id}>{item.title}</li>;
          })} */}
        </ul>
        <br />
      </div>
    );
  }
}
