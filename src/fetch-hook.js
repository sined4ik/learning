import React, { useState, useEffect } from "react";

const FetchHook = () => {
  const [data, setData] = useState([]);
  const [toggleButton, setToggleButton] = useState(false);
  const [name, setName] = useState("");
  const fetchApi = async () => {
    try {
      const data = await fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: {
          id: 1,
          name: name
        }
      });
      const normalizeData = await data.json();
      setData(normalizeData);
    } catch (error) {}
  };
  useEffect(() => {
    fetchApi();
  }, [fetchApi]);

  useEffect(() => {
    console.log("catch toggle button:", toggleButton);
  }, [toggleButton]);

  const toggled = () => {
    setToggleButton(!toggleButton);
  };

  const handleChange = event => {
    setName(event.target.value);
  };

  console.log("==============");
  console.log("name", name);

  return (
    <>
      <h1>{name}</h1>
      <input onChange={handleChange} value={name} />
      {/* <ul>
        {data.map(item => (
          <li key={item.id}>{item.title}</li>
        ))}
      </ul> */}
      <button onClick={toggled}>Toggle</button>
    </>
  );
};

export default FetchHook;
